var Escuela = (function(){
    var _alumnos=[];
    var _docentes=[];
    var _grupos=[];
    var _materias=[];
    
    _alumnos=[{"id":10001,"numero_de_control":11111,"nombre":"Ruben","apellido_paterno":"Guzman", "apellido_materno":"Gomez","calificaciones":[]}]
    _docentes=[{"id":20001,"rfc":"TRYWTUQYETRQ","nombre":"Antonio","apellido_paterno":"Hernandez","apellido_materno":"Blas","grupos":[1234,2345,5678,89901,11111],"materias":[]}]
    _grupos=[{"id":30001,"clave":"GR123","alumnos":[],"docentes":[],"materias":[]}]
    _materias=[{"id":40001,"clave":"MT999","nombre":"español"}];
    
    function _agregar_docente_a(obj){
        /*dd_docente, id_grupo, id_materia*/
        var busqueda=_por_cada_docente(
            function (docente){
                if(docente.id===obj.id_docente){
                    return true;
                }else{
                    return false;
                }        
            }
        );
        if(busqueda.length===0){
            return "No se encuentra ese docente";
        }
        var docente=busqueda[0];
        busqueda=_por_cada_grupo(
            function(grupo){
                if(grupo.id===obj.id_grupo){
                    return true;
                }else{
                    return false;
                }
            }
        );
        if(busqueda.length===0){
            return "No se encuentra el grupo";
        }
        var grupo=busqueda[0];
        
        if(grupo.materias.length>=7){
            return "El grupo ya tiene 7 materias";
        }
        
        busqueda=_por_cada_materia(
            function(materia){
                if(materia.id===obj.id_materia){
                    return true;
                }
                else return false;
            }
            );
        if(busqueda.length===0){
            return "No se encuentra esa materia";
        }

        if(docente.grupos.length>=5){
            return "Muchos grupos para un solo docente";
        }else{
            if(!_buscar_materia_en({"lugar":grupo,"id_materia":obj.id_materia})){
                docente.grupos.push(obj.id_grupo);
                docente.materias.push(obj.id_materia);
                grupo.docentes.push(obj.id_docentes);
                
            }
            else{
                return "Esa materia ya se encuentra en el grupo";
            }
        }
        
        
    }
    
    function _buscar_materia_en(obj){
        /* lugar, id_materia*/
        for(var i=0; i<obj.lugar.materias.length; i++){
            if(obj.lugar.materias[i]===obj.id_materia){
                return true;
            }
        }
        return false;
        
    }
    
    
    function _por_cada_grupo(f){
        var tmp=[];
        for(var i=0; i<_docentes.length; i++){
            if(f(_grupos[i])){
                tmp.push(_grupos[i]);
            }
        }
        return tmp;
    }
    
    function _por_cada_docente(f){
        var tmp=[];
        for(var i=0; i<_docentes.length; i++){
            if(f(_docentes[i])){
                tmp.push(_docentes[i]);
            }
        }
        return tmp;
    }
    function _por_cada_materia(f){
        var tmp=[];
        for(var i=0; i<_materias.length; i++){
            if(f(_materias[i])){
                tmp.push(_materias[i]);
            }
        }
        return tmp;
    }
    
    return {
        
        "agregar_docente_a":_agregar_docente_a,
        "por_cada"  :   _por_cada_docente,
        "buscar_materia_en": _buscar_materia_en
    };
})();


Escuela.agregar_docente_a({"id_docente":20001,"id_grupo":30001,"id_materia":40001});
